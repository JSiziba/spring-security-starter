package com.johnson.security.config;

import com.johnson.security.utility.MillisecondsUtility;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
@Slf4j
public class JwtService {
    private static final String SECRET_KEY = "bb6128ea76eb16a4c918815f364242659d5376dee9e359be17987eb7b60e168c4db691b5414ff1ee25fbc3ef8adf60d9c411a902d19b05ce039d056fc1404a02ab6c720bcbb456d391812e0ec2de7e13364d888f2c7b7eabf1e1c25242e48c9a65ff722fdde74d729e8224c27ba0f818c4fa7dcf28d011d15d8fa55023dae018213fbde270b007290367d6ad63bb70374fd89fbb0233ad4020eefa75295323f9d41d0c0007ba438a05a773f8484475d1bd0b06230e9cbcdd047fd444c6c4c5a2610a5d79b274c4d5483ab71e687e3e17d56006d3359c566f1b5bb5976961189b5455e551854a83647e6f524cc0ac448412beb2329b6acc77adcc7731effb2061";

    public String extractUserEmail(String jwt) {
        return extractClaim(jwt, Claims::getSubject);
    }

    public <T> T extractClaim(String jwt, Function<Claims, T> claimsResolver) {
        final Claims claims = this.extractAllClaims(jwt);
        return claimsResolver.apply(claims);
    }

    public String generateToken(UserDetails userDetails) {
        return this.generateToken(new HashMap<>(), userDetails);
    }

    public boolean isTokenValid(String jwt, UserDetails userDetails) {
        final String userEmail = this.extractUserEmail(jwt);
        return (userEmail.equals(userDetails.getUsername()) && !this.isTokenExpired(jwt));
    }

    private boolean isTokenExpired(String jwt) {
        return this.extractExpiration(jwt).before(new Date());
    }

    private Date extractExpiration(String jwt) {
        return this.extractClaim(jwt, Claims::getExpiration);
    }

    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new java.util.Date(System.currentTimeMillis()))
                .setExpiration(new java.util.Date(System.currentTimeMillis() + MillisecondsUtility.TEN_HOURS_IN_MILLISECONDS))
                .signWith(this.getSigningKey(), io.jsonwebtoken.SignatureAlgorithm.HS256)
                .compact();
    }

    private Claims extractAllClaims(String jwt) {
        return Jwts
                .parserBuilder()
                .setSigningKey(this.getSigningKey())
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }

    private Key getSigningKey() {
        byte[] secretKeyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(secretKeyBytes);
    }
}
