package com.johnson.security.user;

public enum Role {
    USER,
    ADMIN
}
